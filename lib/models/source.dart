class Source {
  String id;
  String name;

  Source(this.id, this.name);

  Source.fromjson(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
  }
}
