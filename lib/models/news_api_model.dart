import 'package:app1/models/news.dart';

class NewsApiModel {
  String status;
  int totalResults;
  List<News> articlesList;

  NewsApiModel(this.status, this.totalResults, this.articlesList);

  NewsApiModel.fromjson(Map<String, dynamic> map) {
    var mapArticles = map['articles'] as List;

    status = map['status'];
    totalResults = map['totalResults'];
    articlesList = mapArticles.map((json) => News.fromjson(json)).toList();
  }
}
