import 'package:app1/models/source.dart';

class News {
  Source source;
  String urlToImage;
  String title;
  String url;
  String author;
  String publishedAt;
  String content;
  String description;

  News(
      {this.source,
      this.urlToImage,
      this.title,
      this.url,
      this.author,
      this.publishedAt,
      this.content,
      this.description});

  News.fromjson(Map<String, dynamic> map) {
    source = Source.fromjson(map['source']);
    author = map['author'];
    title = map['title'];
    description = map['description'];
    url = map['url'];
    urlToImage = map['urlToImage'];
    publishedAt = map['publishedAt'];
    content = map['content'];
  }
}
