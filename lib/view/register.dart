import 'package:app1/util/colors.dart';
import 'package:app1/util/resize.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:app1/util/FireBaseController.dart' as firebaseController;

class RegisterUser extends StatefulWidget {
  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  bool LogButton = false;
  bool eyePass = false;
  TextEditingController _textEditingControllerEmail =
      new TextEditingController();
  TextEditingController _textEditingControllerPassword =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Container(
            padding: EdgeInsets.only(top: 20, left: 10, right: 10),
            height: MediaQuery.of(context).size.height / 1.4,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/img/backbit.png'),
                colorFilter:
                    ColorFilter.mode(Colors.white, BlendMode.softLight),
              ),
            ),
            child: Container(
              height: 100,
              width: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        top: ResizeH(_height, 40), left: 25, right: 25),
                    height: _height * 0.75,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 15,
                        ),
                      ],
                      color: colorBlanco,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(130),
                        bottomRight: Radius.circular(130),
                      ),
                    ),
                    child: Column(
                      children: [
                        Center(
                            child: Image.asset(
                          'assets/img/BitLogo.png',
                          alignment: Alignment.center,
                          width: 140,
                          height: 53,
                          fit: BoxFit.contain,
                        )),

                        Divider(color: Colors.transparent, height: 30),

                        Text('Create your account',
                            style: GoogleFonts.signika(
                                fontSize: 20,
                                color: Colors.blueAccent,
                                fontWeight: FontWeight.normal)),

                        Divider(color: Colors.transparent, height: 20),

                        //Email Field
                        Center(
                          child: Container(
                            padding: EdgeInsets.only(left: 0, right: 0),
                            margin: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 15,
                            child: TextField(
                              controller: _textEditingControllerEmail,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.email,
                                  color: Colors.grey,
                                ),
                                hintText: 'Email',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                focusColor: Colors.black,
                                fillColor: Colors.black,
                                hoverColor: Colors.black,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                    30,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),

                        //Password Field
                        Center(
                          child: Container(
                            padding: EdgeInsets.only(left: 0, right: 0),
                            margin: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 15,
                            child: TextField(
                              obscureText: eyePass == false ? true : false,
                              controller: _textEditingControllerPassword,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.grey,
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(eyePass == false
                                      ? Icons.remove_red_eye
                                      : Icons.remove_red_eye_outlined),
                                  onPressed: () {
                                    setState(() {
                                      eyePass = !eyePass;
                                    });
                                  },
                                ),
                                hintText: 'Password',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                focusColor: Colors.black,
                                fillColor: Colors.black,
                                hoverColor: Colors.black,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                    30,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),

                        //Sign Up Button
                        Center(
                          child: InkWell(
                              onTap: () {
                                setState(() {
                                  //LogButton = !LogButton;
                                  firebaseController.registerUser(
                                    email: _textEditingControllerEmail.text,
                                    password:
                                        _textEditingControllerPassword.text,
                                    context: context,
                                  );
                                  firebaseController.stateChanges(context);
                                });
                              },
                              hoverColor: Colors.red,
                              focusColor: Colors.red,
                              child: Container(
                                padding: EdgeInsets.only(left: 0, right: 0),
                                margin: EdgeInsets.all(10),
                                width: 250,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(90)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        spreadRadius: 1,
                                        blurRadius: 12)
                                  ],
                                ),
                                child: Center(
                                    child: LogButton != true
                                        ? SizedBox(
                                            width: 250,
                                            height: 50,
                                            child: RaisedButton(
                                              onPressed: () {
                                                Navigator.pushNamed(
                                                    context, '/register');
                                              },
                                              child: Text(
                                                'SIGN UP',
                                                style: GoogleFonts.signika(
                                                    fontSize: 24,
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                textAlign: TextAlign.center,
                                              ),
                                              color: Colors.blue,
                                              shape: RoundedRectangleBorder(
                                                side: BorderSide(
                                                    color: Colors.white),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        : CircularProgressIndicator()),
                              )),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
