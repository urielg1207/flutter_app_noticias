import 'package:app1/util/colors.dart';
import 'package:app1/util/resize.dart';
import 'package:app1/view/widgets/app_button.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:app1/util/FireBaseController.dart' as firebaseController;

import '../util/colors.dart';
import '../util/styles.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool eyePass = false;

  TextEditingController _tecEmail = new TextEditingController();
  TextEditingController _tecPassword = new TextEditingController();

  @override
  void initState() {
    initializeApp();
  }

  void initializeApp() async {
    await Firebase.initializeApp();
  }

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/img/backbit.png'),
          colorFilter: ColorFilter.mode(Colors.white, BlendMode.softLight),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                //top: 40,
                top: ResizeH(_height, 40),
                left: 25,
                right: 25),
            height: _height * 0.75,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  blurRadius: 15,
                ),
              ],
              color: colorBlanco,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(130),
                bottomRight: Radius.circular(130),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Welcome to",
                  style: GoogleFonts.signika(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: ResizeH(
                      _height,
                      20,
                    ),
                  ),
                ),
                Image.asset('assets/img/BitLogo.png',
                    width: ResizeH(_height, 157),
                    height: ResizeH(_height, 63),
                    fit: BoxFit.contain),
                Text(
                  'Please login to continue',
                  style: GoogleFonts.signika(
                    fontSize: ResizeH(_height, 20),
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: _height * 0.03,
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextField(
                    controller: _tecEmail,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.grey,
                      ),
                      hintText: 'Username',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      focusColor: Colors.black,
                      fillColor: Colors.black,
                      hoverColor: Colors.black,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                          30,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: _height * 0.02,
                ),
                Align(
                    alignment: Alignment.center,
                    child: TextField(
                      obscureText: eyePass == false ? true : false,
                      controller: _tecPassword,
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.grey,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(eyePass == false
                              ? Icons.remove_red_eye
                              : Icons.remove_red_eye_outlined),
                          onPressed: () {
                            setState(() {
                              eyePass = !eyePass;
                            });
                          },
                        ),
                        hintText: 'Password',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        focusColor: Colors.black,
                        fillColor: Colors.black,
                        hoverColor: Colors.black,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            30,
                          ),
                        ),
                      ),
                    )),
                SizedBox(
                  height: _height * 0.02,
                ),
                Align(
                    alignment: Alignment.centerRight,
                    child: Text('Forgot password ?',
                        style: GoogleFonts.signika(
                          fontSize: ResizeH(_height, 15),
                          color: Colors.lightBlue,
                          fontWeight: FontWeight.normal,
                        ))),
                SizedBox(
                  height: _height * 0.04,
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 250,
                    height: 50,
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          if (_tecEmail.text.isEmpty ||
                              _tecPassword.text.isEmpty) {
                            firebaseController.showMaterialDialog(
                                context: context,
                                message: 'Favor, Ingrese Username y Password');
                          } else {
                            firebaseController.signIn(
                                email: _tecEmail.text,
                                password: _tecPassword.text,
                                context: context);
                            firebaseController.stateChanges(context);
                          }
                        });
                      },
                      child: Text(
                        'LOGIN',
                        style: GoogleFonts.signika(
                            fontSize: 24,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.grey),
                        borderRadius: BorderRadius.all(
                          Radius.circular(
                            20,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 500,
            child: SizedBox(),
          ),
          Text(
            'OR',
            style: Styles.secondaryTextStyle,
          ),
          Expanded(
            flex: 1000,
            child: SizedBox(),
          ),
          SizedBox(
            width: 250,
            height: 50,
            child: RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/register');
              },
              child: Text(
                'SIGN UP',
                style: GoogleFonts.signika(
                    fontSize: 24,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    20,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1000,
            child: SizedBox(),
          ),
        ],
      ),
    )));
  }
}
