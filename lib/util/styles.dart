import 'package:flutter/material.dart';

class Styles {
  static const primaryTextStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: Colors.yellow,
  );

  static const drawerOptionStyle = TextStyle(
      fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey);

  static const secondaryTextStyle = TextStyle(
      fontSize: 28, fontWeight: FontWeight.bold, color: Colors.black87);
}
