import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

void stateChanges(context) async {
  FirebaseAuth.instance.authStateChanges().listen((User user) {
    if (user == null) {
      print('Usuario desconectado!');
    } else {
      print('Usuario conectado!');
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushNamed(context, '/home', arguments: user);
      });
    }
  });
}

void signIn(
    {@required String email,
    @required String password,
    @required context}) async {
  try {
    UserCredential userCredential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      showMaterialDialog(
          context: context, message: 'Usuario no encontrado para este Email');
    } else if (e.code == 'wrong-password') {
      showMaterialDialog(
          context: context, message: 'Contraseña incorrecta para este Usuario');
    }
  }
}

showMaterialDialog({@required context, @required String message}) {
  showDialog(
      context: context,
      builder: (_) => new AlertDialog(
            title: Text("Información"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text('Cerrar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ));
}

void registerUser(
    {@required String email,
    @required String password,
    @required context}) async {
  try {
    UserCredential userCredential = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      showMaterialDialog(
          context: context, message: 'Contraseña ingresada muy débil');
    } else if (e.code == 'email-already-in-use') {
      showMaterialDialog(
          context: context,
          message: 'Este usuario ya se encuentra registrado!');
    }
  } catch (e) {
    print(e);
  }
}

void signOut() async {
  await FirebaseAuth.instance.signOut();
}
